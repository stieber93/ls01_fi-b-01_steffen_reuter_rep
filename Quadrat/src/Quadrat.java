import java.util.Scanner;
public class Quadrat {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Geben Sie die Seitenlšnge des Quadrates ein: ");
		int seitenlaenge = eingabe.nextInt();
		
		for(int i = 1; i <= seitenlaenge; i ++) {
			System.out.print(" *");
		}
		System.out.println();
		
		for(int i = 1; i < seitenlaenge - 1; i ++) {
			System.out.print(" *");
			for(int j = 1; j <= seitenlaenge - 2; j++) {
				System.out.print("  ");
			}
			System.out.println(" *");
		}
		
		for(int i = 0; i < seitenlaenge; i ++) {
			System.out.print(" *");
		}
		

	}

}
