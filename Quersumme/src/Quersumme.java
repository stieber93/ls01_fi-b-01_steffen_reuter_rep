import java.util.Scanner;
public class Quersumme {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		int zahl;
		int summe = 0;
		
		System.out.println("Geben Sie die Zahl ein, aus der die Quersumme berechnet werden soll: ");
		zahl = eingabe.nextInt();
		
		while(zahl > 0) {
			summe = summe + (zahl % 10);
			zahl /= 10;
		}
		System.out.println("Die Quersumme ist: " + summe);

	}

}
