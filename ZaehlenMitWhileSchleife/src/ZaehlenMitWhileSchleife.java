import java.util.Scanner;
public class ZaehlenMitWhileSchleife {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Geben Sie ein bis zu welcher Zahl gez�hlt werden soll: ");
		int n = eingabe.nextInt();

		int i = 1;
		while(i <= n) {
			System.out.print(i + " ");
			i++;
		}
		System.out.println();
		System.out.println();
		System.out.println("Geben Sie eine Zahl ein, von der runtergez�hlt werden soll: ");
		int m = eingabe.nextInt();

		int j = m;
		while(j >= 1) {
			System.out.print(j + " ");
			j--;
		}

	}

}
