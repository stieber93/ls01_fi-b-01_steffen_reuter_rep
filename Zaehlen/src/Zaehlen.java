import java.util.Scanner;

public class Zaehlen {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		System.out.println("Geben Sie ein bis zu welcher Zahl gez�hlt werden soll: ");
		int n = eingabe.nextInt();

		for (int i = 1; i <= n; i++) {
			System.out.println(i);
		}

		System.out.println("Geben Sie eine Zahl ein, von der runtergez�hlt werden soll: ");
		int m = eingabe.nextInt();

		for (int i = m; i >= 1; i--) {
			System.out.println(i);
		}

	}

}
