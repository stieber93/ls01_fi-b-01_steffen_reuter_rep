import java.util.Scanner;

public class Taschenrechner {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		
		double zahl1;
		double zahl2;
		double ergebnis;
		char operator;
		
		System.out.println("Geben Sie die erste Zahl ein: ");
		zahl1 = eingabe.nextDouble();
		
		System.out.println("Geben Sie die zweite Zahl ein: ");
		zahl2 = eingabe.nextDouble();
		
		System.out.println("Wollen Sie die Zahlen addieren, subtrahieren, multiplizieren oder dividieren? ");
		operator = eingabe.next().charAt(0);
		
		switch (operator) {
		case '+':
			ergebnis = zahl1 + zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
			break;
			
		case '-':
			ergebnis = zahl1 - zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
			break;
			
		case '*':
			ergebnis = zahl1 * zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
			break;
			
		case '/':
			ergebnis = zahl1 / zahl2;
			System.out.println("Das Ergebnis ist: " + ergebnis);
			break;
			
		default:
			System.out.println("Diese Eingabe ist nicht g�ltig! ");
			break;
		}

	}

}
