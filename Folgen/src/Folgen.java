
public class Folgen {

	public static void main(String[] args) {
		
		System.out.println("a)");
		for(int i = 99; i >= 9; i -= 3) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.println("b)");
		for(int i = 1; i <= 20; i = i + 1) {
			System.out.print(i * i + " ");
		}
		System.out.println();
		System.out.println("c)");
		for(int i = 2; i <= 102; i += 4) {
			System.out.print(i + " ");
		}
		System.out.println();
		System.out.println("d)");
		for(int i = 2; i <= 32; i += 2) {
			System.out.print(i * i + " ");
		}
		System.out.println();
		System.out.println("e)");
		for(int i = 2; i <= 32768; i *= 2) {
			System.out.print(i + " ");
		}

	}

}
