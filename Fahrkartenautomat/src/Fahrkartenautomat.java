import java.util.Scanner;
public class Fahrkartenautomat {

	static Scanner mytastatur = new Scanner(System.in);

	public static void main(String[] args) {
		do {
			double zuZahlenderBetrag;
			double eingezahlterGesamtbetrag;
			double eingeworfeneM�nze;
			double r�ckgabebetrag;
			int anzahlTickets;
			System.out.println("Fahrkartenbestellvorgang: ");
			System.out.println("=========================\n");
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
	
			// Geldeinwurf
			// -----------
			eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlenderBetrag);
	
			// Fahrscheinausgabe
			// -----------------
			fahrkartenAusgeben();
	
			// R�ckgeldberechnung und -Ausgabe
			// -------------------------------
	
			rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlenderBetrag);
	
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.");
		} while (true);
		
	}
	/***
	 * Diese Methode verwenden wenn Aufgabe 5 ausgef�hrt werden soll.
	 * 
	 * @return gesamtpreis
	 */
	public static double fahrkartenbestellungErfassen_aufgabe5() {
		int anzahlTickets;
		do {
			System.out.println("Sie m�ssen einen Wert zwischen 1 und 10 eingeben. ");
			System.out.println("Wie viele Karten m�chten Sie kaufen");
			anzahlTickets = mytastatur.nextInt();
		} while (anzahlTickets < 1 || anzahlTickets > 10);
		System.out.println("Zu zahlender Betrag pro Ticket (Euro)");
		
		double preis = mytastatur.nextDouble();
		
		double gesamtpreis = anzahlTickets * preis;
		
		return gesamtpreis;
		
	}
	//abc
	public static double fahrkartenbestellungErfassen() {
		int ticketwahl;
		double gesamtpreis = 0;
		double einzelfahrschein = 2.9;
		double tageskarte = 8.6;
		double kleingruppenTageskarte = 23.5;
		do {
			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
			System.out.println("\tEinzelfahrschein Regeltarif AB [2,90 EUR] (1) ");
			System.out.println("\tTageskarte Regeltarif AB [8,60 EUR] (2) ");
			System.out.println("\tKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3) ");
			System.out.println("\tBezahlen (9) ");
			

			System.out.println("Ihre Wahl: ");
			ticketwahl = mytastatur.nextInt();
			while(ticketwahl < 1 || (ticketwahl > 3 && ticketwahl != 9)) {
				System.out.println("\t>>falsche Eingabe<< ");
				System.out.println("Bitte w�hlen Sie einen der genannten drei Fahrscheine indem Sie die entsprechende Ziffer [1-3] eingeben oder die Ziffer [9] zum bezahlen eingeben. ");
				System.out.println("Ihre Wahl: ");
				ticketwahl = mytastatur.nextInt();
			} 
			if(ticketwahl == 9) {
				return gesamtpreis;
			}
			
			int anzahlTickets;
			do {
				System.out.println("Anzahl der Tickets: ");
				anzahlTickets = mytastatur.nextInt();
				if(anzahlTickets <1) {
					System.out.println("Die eingegebene Anzahl ist ung�ltig. Bitte w�hlen Sie mindestens 1 Ticket. ");
				}
			} while (anzahlTickets < 1);
			
			double preis;
			switch (ticketwahl) {
			case 1: preis = einzelfahrschein;
				break;
			case 2: preis = tageskarte;
				break;
			case 3: preis = kleingruppenTageskarte;
				break;
			default: preis = 0;
				break;
			}
			
			gesamtpreis = gesamtpreis + (anzahlTickets * preis);
		} while(true);
		
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfeneM�nze;
		double eingezahlterGesamtbetrag = (double) 0.0;
		while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
			System.out.printf("Noch zu zahlen: %.2f \n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = mytastatur.nextFloat();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;

		}
		return eingezahlterGesamtbetrag;

	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		warte(250);
			}
		
	
	public static void warte(int millisekunde) {
		try {
			Thread.sleep(millisekunde);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace(); }
	}

	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlenderBetrag) {
		double r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if (r�ckgabebetrag > 0.0) {
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f %s \n", r�ckgabebetrag, " EURO");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				muenzeAusgeben(2, "Euro");
				r�ckgabebetrag -= 2.0;
			}
			while (r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				muenzeAusgeben(1, "Euro");
				r�ckgabebetrag -= 1.0;
			}
			while (r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				muenzeAusgeben(50, "Cent");
				r�ckgabebetrag -= 0.5;
			}
			while (r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				muenzeAusgeben(20, "Cent");
				r�ckgabebetrag -= 0.2;
			}
			while (r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				muenzeAusgeben(10, "Cent");
				r�ckgabebetrag -= 0.1;
			}
			while (r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				muenzeAusgeben(5, "Cent");
				r�ckgabebetrag -= 0.05;
			}
		
		}
		
	}
	
	public static void muenzeAusgeben(int betrag, String einheit) {
		System.out.println(betrag + " " + einheit);
	}
}