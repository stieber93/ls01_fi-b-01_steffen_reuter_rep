import java.util.Scanner;

public class Ohm {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		
		char ohmWert;
		double wert1;
		double wert2;
		double ergebnis;
		
		System.out.println("Welchen Wert des ohmschen Gesetzes (R, U oder I) m�chten Sie ausrechnen? ");
		ohmWert = eingabe.next().charAt(0);
		
		switch (ohmWert) {
		case 'R':
			System.out.println("Geben Sie den Wert der Spannung (U) ein: ");
			wert1 = eingabe.nextDouble();
			System.out.println("Geben Sie den Wert der Stromst�rke (I) ein: ");
			wert2 = eingabe.nextDouble();
			ergebnis = wert1 / wert2;
			System.out.println("Der Widerstand betr�gt: " + ergebnis + " Ohm");
			break;
			
		case 'U':
			System.out.println("Geben Sie den Wert des Widerstandes (R) ein: ");
			wert1 = eingabe.nextDouble();
			System.out.println("Geben Sie den Wert der Stromst�rke (I) ein: ");
			wert2 = eingabe.nextDouble();
			ergebnis = wert1 * wert2;
			System.out.println("Die Spannung betr�gt: " + ergebnis + " Volt");
			break;
			
		case 'I':
			System.out.println("Geben Sie den Wert der Spannung (U) ein: ");
			wert1 = eingabe.nextDouble();
			System.out.println("Geben Sie den Wert des Widerstandes (R) ein: ");
			wert2 = eingabe.nextDouble();
			ergebnis = wert1 / wert2;
			System.out.println("Die Stromst�rke betr�gt: " + ergebnis + " Ampere");
			break;
			
		default:
			System.out.println("Der eingegebene Wert ist nicht g�ltig. ");
		}
		

	}

}
