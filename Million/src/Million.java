import java.util.Scanner;

public class Million {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {
		double einlage;
		double zinssatz;
		int zaehler = 1; //um die Jahre zu z�hlen
		char auswahl;
		boolean neustart = true;
		
		while(neustart == true) {
		System.out.println("Bitte geben Sie die h�he Ihrer Einlage ein: ");
		einlage = eingabe.nextDouble(); // eingabe der Einlage
		
		System.out.println("Bitte geben Sie die h�he des Zinssatzes ein: ");
		zinssatz = eingabe.nextDouble() + 1; // eingabe des Zinssatzes
		
		double i = einlage;
		while(i <= 1000000) {
			System.out.println(i);
			i *= zinssatz;
			zaehler++;
		}
		
		System.out.println(zaehler + " Jahre bis zur Million. ");
		System.out.println("M�chten Sie das Programm mit anderen Werten erneut durchf�hren? [j = ja | n = nein] ");
		auswahl = eingabe.next().charAt(0);
		
		if(auswahl == 'j') {
			neustart = true;
		}
		else {
			System.out.println("Das Programm wird beendet. ");
			neustart = false;
		}
		}
		

	}

}
