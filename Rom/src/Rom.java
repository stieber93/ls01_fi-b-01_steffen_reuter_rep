import java.util.Scanner;

public class Rom {
	static Scanner eingabe = new Scanner(System.in);

	public static void main(String[] args) {

		char römischesZeichen;
		String dezimalzahl = "";

		System.out.println("Geben Sie ein römisches Zahlenzeichen ein: ");
		römischesZeichen = eingabe.next().charAt(0);

		switch (römischesZeichen) {
		case 'I':
			dezimalzahl = "1";
			break;

		case 'V':
			dezimalzahl = "5";
			break;

		case 'X':
			dezimalzahl = "10";
			break;

		case 'L':
			dezimalzahl = "50";
			break;

		case 'C':
			dezimalzahl = "100";
			break;

		case 'D':
			dezimalzahl = "500";
			break;

		case 'M':
			dezimalzahl = "1000";
			break;

		default:
			System.out.println(römischesZeichen + " ist kein römisches Zeichen");
			break;
		}
		System.out.println("Ihre römische Zahl entspricht der Ziffer: " + dezimalzahl);

	}

}
